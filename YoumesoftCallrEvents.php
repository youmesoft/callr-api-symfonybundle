<?php

namespace Youmesoft\CallrBundle;

final class YoumesoftCallrEvents
{
    const CALLR_REQUEST = 'callr.request';
    const CALLR_SMS_SEND = 'callr.sms.send';
}
